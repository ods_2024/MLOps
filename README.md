# MLOps



## Getting started

The main proposal - to keep going through learning course from `Open Data Science` team, seson spring-2024, hit the score and commit with end-to-end solution (to be continued)

## Strategy

The core idea is to evolve the solid pet-project hands-on. To handle it by oneself I chosed the *Data Science Lifecycle Process* methodology as the best fitted to my goals.

## The process

**To get kown with application's run pipeline follow the `contributing.md` instructions**

## Roadmap

1. Codestyle
2. Package management
3. Templates and dependency management
4. Snakemake + Hydra
5. DVC + LakeFS
6. Research tracking tools
7. Code and data testing methods and tools
8. Docker containers
9. Streamlit
10. Kubernetes
11. FeatureStore
12. Monitoring and analytics of ML models
13. Process orchestration

## Contributing
Fill free to contribute and Follow the `contributing.md` inastructions.

## Authors and acknowledgment
Anton Shkvarskiy, ODS team

## License
MIT License

## Project status
Stay tuned to get the updates! 
