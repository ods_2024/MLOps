This repository was created by Anton Shkvarskiy, a student of the MLOps course (OpenDataScience, Spring 2024), for managing an individual project and for checking task completion.
You'll find out some linters and formatters usage instructions hovered over at `pyproject.toml`. The aim is to keep code clean, readable and ready to fix.
1. So, the first helper of essentials is `Black`, a package to reduce a misunderstanding while team mates are running with code.
2. `Flake8` is to enforce the style guide.
3. `Ruff`, the last but not least, to suggest that code is able to be refactored. 

To start the process use commands via terminal:
- First step is to make sure you have an actual version of `pre-commit` 

```
pip install pre-commit
```

- So after that, go through the `.pre-commit-config.yaml` where all the hooks described formatters and linters installation
- And as a final steps:
```
pre-commit install
pre-commit run --all-files
```

So, the next big thing is to create an environment with python 3.11 or more actual version with running:
```
docker build -t mlops_app ./app
docker run -it mlops_app
```
Please check up you are running current venv.

The main app file `app.py` is a toy example, will be complited according the course progress. 